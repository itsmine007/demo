FROM rocker/rstudio:3.6.2
RUN apt-get update
RUN apt-get install -y libxml2-dev
RUN apt-get install -y libcairo2-dev
RUN apt-get install -y libsodium-dev
Run apt-get install -y git
Run apt install -y curl
RUN Rscript -e "install.packages('shinythemes',dependencies=TRUE,repos='http://cran.rstudio.com')"
RUN Rscript -e "install.packages('boot',dependencies=TRUE,repos='http://cran.rstudio.com')"
RUN Rscript -e "install.packages('pander',dependencies=TRUE,repos='http://cran.rstudio.com')"

